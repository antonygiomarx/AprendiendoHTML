// Objetos.js

// Variables generales
var ClienteId = '';
var AbonadoId = '';
var CuentaId = '';
var htmlReparacion = '';
var claseReparacion = '';
var htmlRecaudacion = '';
var claseRecaudacion = '';
var htmlTerminal = '';
var claseTerminal = '';
var htmlFacturacion = '';
var claseFacturacion = '';
var htmlUltimasInt = '';
var claseUltimasInt = '';
var htmlContenido = '';
var htmlPrincipal = '';
var busquedaCompleta = 0;
var limpiarReperacion = 0;
var limpiarRecaudacion = 0;
var limpiarTerminal = 0;
//TERMINAL TRAFICO JCASTELLANOS DIC/2015
var limpiarTerminalTrafico = 0;
var htmlTerminalTrafico = '';
var claseTerminalTrafico = '';
//
var limpiarFacturacion = 0;
var limpiarUltimasInt = 0;
var srcUltimasInt = '';
var codPlanTarifario = '';
var codPlanTarifarioAbo = '';
// Variables generales



function ObtenerValor(elemento) {
    var retorno = '';
    retorno = elemento[0].textContent;
    if (retorno == null) {
        retorno = elemento[0].text;
    }

    return retorno;
}


function LimpiarEtiquetas() {
    document.getElementById('lbNombre').innerHTML = '';
    document.getElementById('lbNoCuenta').innerHTML = '';
    document.getElementById('lbResponsable').innerHTML = '';
    document.getElementById('lbRuc').innerHTML = '';
    document.getElementById('lbTipoRuc').innerHTML = '';
    document.getElementById('lbVendedor').innerHTML = '';
    document.getElementById('lbTipo').innerHTML = '';
    document.getElementById('lbClasificacion').innerHTML = '';
    document.getElementById('lbCAVCuenta').innerHTML = '';

    document.getElementById('lbNombreCliente').innerHTML = '';
    document.getElementById('lbNoCliente').innerHTML = '';
    document.getElementById('lbCedula').innerHTML = '';
    document.getElementById('lbTipoCedula').innerHTML = '';
    document.getElementById('lbAbonado').innerHTML = '';
    document.getElementById('lbCiclo').innerHTML = '';
    document.getElementById('lbTelefonos').innerHTML = '';
    document.getElementById('lbCav').innerHTML = '';
    //document.getElementById('lbClub').innerHTML = '';
    document.getElementById('lbProducto').innerHTML = '';
    document.getElementById('lbCumple').innerHTML = '';
    document.getElementById('edComentario').value = '';
    ClienteId = '';

    document.getElementById('lbNombreAbonado').innerHTML = '';
    document.getElementById('lbNumeroAbonado').innerHTML = '';
    document.getElementById('lbTecnologia').innerHTML = '';
    document.getElementById('lbSituacion').innerHTML = '';
    document.getElementById('lbEstadoLTE').innerHTML = '';
    document.getElementById('lbPlanTarifario').innerHTML = '';
    document.getElementById('lbFechaAlta').innerHTML = '';
    document.getElementById('lbFechaExp').innerHTML = '';
    document.getElementById('lbCualitativo').innerHTML = '';
    document.getElementById('lbCAVAbonado').innerHTML = '';
    document.getElementById('lbEmailCAVAbonado').innerHTML = '';



    if (limpiarFacturacion == 0) {
        document.getElementById('lbProvincia').innerHTML = '';
        document.getElementById('lbDistrito').innerHTML = '';
        document.getElementById('lbCorregimiento').innerHTML = '';
        //document.getElementById('lbActualizado').innerHTML = '';
        document.getElementById('lbCasilla').innerHTML = '';
        document.getElementById('lbCalle').innerHTML = '';
        document.getElementById('lbCasa').innerHTML = '';
        //document.getElementById('lbApartado').innerHTML = '';
        document.getElementById('lbEstafeta').innerHTML = '';
        document.getElementById('lbCorreo').innerHTML = '';
        CerrarFacturacion();
    }

    if (limpiarTerminal == 0) {
        document.getElementById('lbMarca').innerHTML = '';
        //document.getElementById('lbICC').innerHTML = '';

        document.getElementById('lbICCModeloTecPlan').innerHTML = '';
        document.getElementById('lbImei').innerHTML = '';
        document.getElementById('lbPrecioRegular').innerHTML = '';
        document.getElementById('lbPrecioVenta').innerHTML = '';
        document.getElementById('lbSubsidio').innerHTML = '';
        document.getElementById('lbSiniestro').innerHTML = '';
        document.getElementById('lbAsegurado').innerHTML = '';
        document.getElementById('lbFechaCambio').innerHTML = '';
        CerrarTerminal();
    }
    if (limpiarTerminalTrafico == 0) {
        document.getElementById('lbMarcaTrafico').innerHTML = '';
        document.getElementById('lbImeiTrafico').innerHTML = '';
        document.getElementById('lbProcedencia').innerHTML = '';
        document.getElementById('lbtecnologiaTrafico').innerHTML = '';
        document.getElementById('lbBandaLte').innerHTML = '';
        CerrarTerminalTrafico();
    }

    if (limpiarRecaudacion == 0) {
        document.getElementById('lbSaldoCuenta').innerHTML = '';
        document.getElementById('lbSaldoCliente').innerHTML = '';
        document.getElementById('lbSaldoAbonado').innerHTML = '';
        document.getElementById('lbFechaUltimo').innerHTML = '';
        document.getElementById('lbUltimoPago').innerHTML = '';
        CerrarRecaudacion();
    }

    if (limpiarUltimasInt == 0) {
        CerrarUltimasInt();
    }

    document.getElementById('edContacto').value = '';
    document.getElementById('edNombre').value = '';
    document.getElementById('comentariosRech').value = '';
    document.getElementById('cmbMotivoRechazo').value = '';
    document.getElementById('comentariosAcept').value = '';
    promosHTML = '<div>El cliente no tiene promociones activas</div>';
    jQuery('#lnkPromo').css('display', 'none');

    // Se agregan los datos de segmento valor 15/07/2015 
    document.getElementById('lblSegmentoValor').innerHTML = '';
    // FIN datos de segmento valor 15/07/2015 

}

function DatosCuenta(xml) {
    document.getElementById('lbNombre').innerHTML = ObtenerValor(xml[0].getElementsByTagName('nombre'));
    document.getElementById('lbNoCuenta').innerHTML = ObtenerValor(xml[0].getElementsByTagName('numero'));
    document.getElementById('lbResponsable').innerHTML = ObtenerValor(xml[0].getElementsByTagName('responsable'));
    document.getElementById('lbRuc').innerHTML = ObtenerValor(xml[0].getElementsByTagName('identificacion'));
    document.getElementById('lbTipoRuc').innerHTML = ObtenerValor(xml[0].getElementsByTagName('tipoidentificacion'));
    document.getElementById('lbVendedor').innerHTML = ObtenerValor(xml[0].getElementsByTagName('vendedor'));
    document.getElementById('lbTipo').innerHTML = ObtenerValor(xml[0].getElementsByTagName('tipo'));

    var nom_img = ObtenerValor(xml[0].getElementsByTagName('nom_img'));

    if (nom_img != '')
        nom_img = "<IMG	border=\"0\" src=\"" + nom_img + "\" width=\"15\" height=\"15\" id=\"imgClas\">&nbsp;&nbsp;";

    document.getElementById('lbClasificacion').innerHTML = nom_img + ObtenerValor(xml[0].getElementsByTagName('clasificacion'));
    document.getElementById('lbCAVCuenta').innerHTML = ObtenerValor(xml[0].getElementsByTagName('cav'));
    CuentaId = ObtenerValor(xml[0].getElementsByTagName('numero'));
}

function DatosCliente(xml) {
    document.getElementById('lbNombreCliente').innerHTML = ObtenerValor(xml[0].getElementsByTagName('nombre_cliente'));
    document.getElementById('lbNoCliente').innerHTML = ObtenerValor(xml[0].getElementsByTagName('numero_cliente'));
    document.getElementById('lbCedula').innerHTML = ObtenerValor(xml[0].getElementsByTagName('identificacion'));
    document.getElementById('lbTipoCedula').innerHTML = ObtenerValor(xml[0].getElementsByTagName('tipoidentificacion'));
    document.getElementById('lbAbonado').innerHTML = ObtenerValor(xml[0].getElementsByTagName('abonados'));
    document.getElementById('lbCiclo').innerHTML = ObtenerValor(xml[0].getElementsByTagName('ciclo'));
    document.getElementById('lbTelefonos').innerHTML = ObtenerValor(xml[0].getElementsByTagName('telefonos'));
    document.getElementById('lbCav').innerHTML = ObtenerValor(xml[0].getElementsByTagName('cav'));
    //document.getElementById('lbClub').innerHTML = ObtenerValor(xml[0].getElementsByTagName('movistar'));
    document.getElementById('lbProducto').innerHTML = ObtenerValor(xml[0].getElementsByTagName('producto'));
    document.getElementById('edNombre').value = ObtenerValor(xml[0].getElementsByTagName('nombre_cliente'));
    document.getElementById('edContacto').value = ObtenerValor(xml[0].getElementsByTagName('telefonos'));
    document.getElementById('edComentario').value = ObtenerValor(xml[0].getElementsByTagName('comentario'));
    ClienteId = ObtenerValor(xml[0].getElementsByTagName('numero_cliente'));
    if ((ClienteId == null) || (ClienteId == '')) {
        LimpiarEtiquetas();
    }

    if (document.getElementById('cbCriterio').value == 'CL') {
        srcUltimasInt = '../gestiones/UltimasInteracciones.jsp?ID_ABONADO=-1' +
            '&CRITERIO=' + document.getElementById('cbCriterio').value + '&DATO=' + document.getElementById('edValor').value;
        busquedaCompleta = 1;
    }

    // Se agregan los datos de segmento valor 15/07/2015 
    document.getElementById('lblSegmentoValor').innerHTML = ObtenerValor(xml[0].getElementsByTagName('segmento-valor'));
    // FIN datos de segmento valor 15/07/2015 

}

var promosHTML = '';

function DatosAbonado(xml) {
    document.getElementById('lbNombreAbonado').innerHTML = ObtenerValor(xml[0].getElementsByTagName('nombre_abonado'));
    document.getElementById('lbNumeroAbonado').innerHTML = ObtenerValor(xml[0].getElementsByTagName('numero_abonado'));
    document.getElementById('lbTecnologia').innerHTML = ObtenerValor(xml[0].getElementsByTagName('tecnologia'));
    document.getElementById('lbSituacion').innerHTML = ObtenerValor(xml[0].getElementsByTagName('situacion'));
    document.getElementById('lbPlanTarifario').innerHTML = ObtenerValor(xml[0].getElementsByTagName('plan'));
    document.getElementById('lbFechaAlta').innerHTML = ObtenerValor(xml[0].getElementsByTagName('fecha_alta'));
    document.getElementById('lbEstadoLTE').innerHTML = ObtenerValor(xml[0].getElementsByTagName('estadoLTE'));
    //alert(" "+ObtenerValor(xml[0].getElementsByTagName('estadoLTE')));
    if (ObtenerValor(xml[0].getElementsByTagName('vencido')) == '0') {
        document.getElementById('lbFechaExp').innerHTML = ObtenerValor(xml[0].getElementsByTagName('fecha_baja'));
    } else {
        document.getElementById('lbFechaExp').innerHTML = '<font color=red>' + ObtenerValor(xml[0].getElementsByTagName('fecha_baja')) + '</font>';
    }
    document.getElementById('lbCualitativo').innerHTML = ObtenerValor(xml[0].getElementsByTagName('cualitativo'));
    document.getElementById('lbCAVAbonado').innerHTML = ObtenerValor(xml[0].getElementsByTagName('cav'));
    document.getElementById('lbEmailCAVAbonado').innerHTML = ObtenerValor(xml[0].getElementsByTagName('emailcav'));
    codPlanTarifario = ObtenerValor(xml[0].getElementsByTagName('cod_plan'));
    codPlanTarifarioAbo = ObtenerValor(xml[0].getElementsByTagName('numero_abonado'));
    srcUltimasInt = '../gestiones/UltimasInteracciones.jsp?ID_ABONADO=' + ObtenerValor(xml[0].getElementsByTagName('numero_abonado')) +
        '&CRITERIO=AC' + '&DATO=' + document.getElementById('edValor').value;
    document.getElementById('lbCumple').innerHTML = ObtenerValor(xml[0].getElementsByTagName('cumple'));
    busquedaCompleta = 1;

    var fecha = new Date();

    var fecha_str = fecha.getDate() + '/' + (fecha.getMonth() + 1);
    fecha_str = fecha_str.replace('0', '').replace('0', '');

    var fecha_cumple = '';
    if (ObtenerValor(xml[0].getElementsByTagName('cumple')) != null && ObtenerValor(xml[0].getElementsByTagName('cumple')) != '') {

        fecha_cumple = ObtenerValor(xml[0].getElementsByTagName('cumple')).substring(0, 5).replace('0', '').replace('0', '');
    }

    var fecha_aniv = '';
    if (ObtenerValor(xml[0].getElementsByTagName('fecha_alta')) != null && ObtenerValor(xml[0].getElementsByTagName('fecha_alta')) != '') {
        fecha_aniv = ObtenerValor(xml[0].getElementsByTagName('fecha_alta')).substring(0, 5).replace('0', '').replace('0', '');
    }

    if (fecha_str == fecha_cumple) {
        jQuery('#basic-modal-content-cumple').modal();
    } else if (fecha_str == fecha_aniv) {
        jQuery('#basic-modal-content-aniv').modal();
    }
    if (promosHTML != null && promosHTML != '') {
        blink(90000000, 2000);
        jQuery('#lnkPromo').css('display', 'block');
        document.getElementById('basic-modal-content-promo').innerHTML = promosHTML;
        //jQuery('#basic-modal-content-promo').modal();
    } else {
        jQuery('#lnkPromo').css('display', 'none');
    }
}

function InformacionRecaudacion(xml) {
    var rec = document.getElementById('dvRecaudacion');
    rec.innerHTML = htmlRecaudacion;
    rec.className = claseRecaudacion;
    limpiarRecaudacion = 0;

    document.getElementById('lbSaldoCuenta').innerHTML = ObtenerValor(xml[0].getElementsByTagName('saldo_cuenta'));
    document.getElementById('lbSaldoCliente').innerHTML = ObtenerValor(xml[0].getElementsByTagName('saldo_cliente'));
    document.getElementById('lbSaldoAbonado').innerHTML = ObtenerValor(xml[0].getElementsByTagName('saldo_abonado'));
    document.getElementById('lbFechaUltimo').innerHTML = ObtenerValor(xml[0].getElementsByTagName('fecha'));
    document.getElementById('lbUltimoPago').innerHTML = ObtenerValor(xml[0].getElementsByTagName('monto'));
}

function DireccionFacturacion(xml) {
    var fac = document.getElementById('dvFacturacion');
    fac.innerHTML = htmlFacturacion;
    fac.className = claseFacturacion;
    limpiarFacturacion = 0;

    document.getElementById('lbProvincia').innerHTML = ObtenerValor(xml[0].getElementsByTagName('provincia'));
    document.getElementById('lbDistrito').innerHTML = ObtenerValor(xml[0].getElementsByTagName('distrito'));
    document.getElementById('lbCorregimiento').innerHTML = ObtenerValor(xml[0].getElementsByTagName('corregimiento'));
    //document.getElementById('lbActualizado').innerHTML = ObtenerValor(xml[0].getElementsByTagName('actualizado'));
    document.getElementById('lbCasilla').innerHTML = ObtenerValor(xml[0].getElementsByTagName('casilla'));
    document.getElementById('lbCalle').innerHTML = ObtenerValor(xml[0].getElementsByTagName('calle'));
    document.getElementById('lbCasa').innerHTML = ObtenerValor(xml[0].getElementsByTagName('casa'));
    ///document.getElementById('lbApartado').innerHTML = ObtenerValor(xml[0].getElementsByTagName('apartado'));
    document.getElementById('lbEstafeta').innerHTML = ObtenerValor(xml[0].getElementsByTagName('estafeta'));
    document.getElementById('lbCorreo').innerHTML = ObtenerValor(xml[0].getElementsByTagName('correo'));
}


function DatosTerminal(xml) {

    var term = document.getElementById('dvTerminal');
    term.innerHTML = htmlTerminal;
    term.className = claseTerminal;
    limpiarTerminal = 0;

    document.getElementById('lbMarca').innerHTML = ObtenerValor(xml[0].getElementsByTagName('modelo'));
    //document.getElementById('lbICC').innerHTML = ObtenerValor(xml[0].getElementsByTagName('sim'));
    document.getElementById('lbICCModeloTecPlan').innerHTML = ObtenerValor(xml[0].getElementsByTagName('iccTCP'));
    document.getElementById('lbImei').innerHTML = ObtenerValor(xml[0].getElementsByTagName('imei'));
    document.getElementById('lbPrecioRegular').innerHTML = ObtenerValor(xml[0].getElementsByTagName('precio_regular'));
    document.getElementById('lbPrecioVenta').innerHTML = ObtenerValor(xml[0].getElementsByTagName('precio_venta'));
    document.getElementById('lbSubsidio').innerHTML = ObtenerValor(xml[0].getElementsByTagName('subsidio'));
    if (ObtenerValor(xml[0].getElementsByTagName('siniestro')) == '1') {
        document.getElementById('lbSiniestro').innerHTML = '<img src="../imagenes/checkmark.gif">';
    } else {
        document.getElementById('lbSiniestro').innerHTML = '';
    }
    if (ObtenerValor(xml[0].getElementsByTagName('asegurado')) == '1') {
        document.getElementById('lbAsegurado').innerHTML = '<img src="../imagenes/checkmark.gif">';
    } else {
        document.getElementById('lbAsegurado').innerHTML = '';
    }
    document.getElementById('lbFechaCambio').innerHTML = ObtenerValor(xml[0].getElementsByTagName('fecha_cambio'));

}

function CerrarTerminal() {
    var term = document.getElementById('dvTerminal');
    htmlTerminal = term.innerHTML;
    claseTerminal = term.className;
    term.innerHTML = '';
    term.className = '';
    limpiarTerminal = 1;
}
/*
 * DATOS TERMINA EN TRAFICO JCASTELLANOS DIC/2015
 */
function DatosTerminalTrafico(xml) {

    var termt = document.getElementById('dvTerminalTrafico');
    termt.innerHTML = htmlTerminalTrafico;
    termt.className = claseTerminalTrafico;
    limpiarTerminalTrafico = 0;

    document.getElementById('lbMarcaTrafico').innerHTML = ObtenerValor(xml[0].getElementsByTagName('ModeloTrafico'));
    document.getElementById('lbImeiTrafico').innerHTML = ObtenerValor(xml[0].getElementsByTagName('ImeiTrafico'));
    document.getElementById('lbProcedencia').innerHTML = ObtenerValor(xml[0].getElementsByTagName('Procedencia'));
    document.getElementById('lbtecnologiaTrafico').innerHTML = ObtenerValor(xml[0].getElementsByTagName('Tecnologia'));
    document.getElementById('lbBandaLte').innerHTML = ObtenerValor(xml[0].getElementsByTagName('BandaLte'));


}

function CerrarTerminalTrafico() {
    var termt = document.getElementById('dvTerminalTrafico');
    htmlTerminalTrafico = termt.innerHTML;
    claseTerminalTrafico = termt.className;
    termt.innerHTML = '';
    termt.className = '';
    limpiarTerminalTrafico = 1;
}


function CerrarFacturacion() {
    var fac = document.getElementById('dvFacturacion');
    htmlFacturacion = fac.innerHTML;
    claseFacturacion = fac.className;
    fac.innerHTML = '';
    fac.className = '';
    limpiarFacturacion = 1;
}

function CerrarRecaudacion() {
    var rec = document.getElementById('dvRecaudacion');
    htmlRecaudacion = rec.innerHTML;
    claseTerminal = rec.className;
    rec.innerHTML = '';
    rec.className = '';
    limpiarRecaudacion = 1;
}


function CerrarUltimasInt() {
    var ult = document.getElementById('dvCasos');
    htmlUltimasInt = ult.innerHTML;
    claseUltimasInt = ult.className;
    ult.innerHTML = '';
    ult.className = '';
    limpiarUltimasInt = 1;
}

function CerrarPestanas() {
    var rec = document.getElementById('dvRecaudacion');
    htmlRecaudacion = rec.innerHTML;
    claseRecaudacion = rec.className;
    rec.innerHTML = '';
    rec.className = '';
    limpiarRecaudacion = 1;

    var rep = document.getElementById('dvReparacion');
    htmlReparacion = rep.innerHTML;
    claseReparacion = rep.className;
    rep.innerHTML = '';
    rep.className = '';
    limpiarReperacion = 1;

    CerrarTerminal();
    CerrarTerminalTrafico();
    CerrarFacturacion();
    CerrarUltimasInt();

    document.getElementById('edValor').focus();
}


function ProcesaTelefono(xml) {
    operacion = '';
    if (ObtenerValor(xml[0].getElementsByTagName('te')) == 'S') {
        var Dato = document.getElementById('edValor').value;
        var tipoDato = document.getElementById('cbCriterio').value;
        BuscarTelefono(tipoDato, Dato);
    } else {
        Mensaje('No existe informacion del telefono ingresado');
    }
}


function TelefonoExiste(tdt, dt) {
    operacion = 'Telefono Existe';
    sendRequest('../ObtenerDatos', 'op=telex&td=' + tdt + '&d=' + dt, 'POST');
}


function BuscarTelefono(tdt, dt) {
    operacion = 'Datos Generales';
    sendRequest('../ObtenerDatos', 'op=dash&td=' + tdt + '&d=' + dt, 'POST');
}

function ValidarTelefono(cadTel) {
    operacion = 'Validar Telefono';
    sendRequest('../ObtenerDatos', 'op=tv&t=' + cadTel, 'POST');
}


function TelefonoValido(xml) {
    if (ObtenerValor(xml[0].getElementsByTagName('codigo-error')) == '0') {
        var Dato = document.getElementById('edValor').value;
        var tipoDato = document.getElementById('cbCriterio').value;
        var valido = xml[0].getElementsByTagName('valido');

        if (tipoDato == 'P') {
            document.location = '../gestiones/inicioProspecto.jsp?pContacto=' + Dato;
        } else {
            if (ObtenerValor(valido) == 'S') {
                tipoDato = document.getElementById('cbCriterio').value;
                //Si es Telefono
                if (tipoDato == 'T') {
                    operacion = '';
                    TelefonoExiste(tipoDato, Dato);
                    //(tipoDato, Dato);
                }
                if (tipoDato == 'P') {
                    document.location = '../gestiones/inicioProspecto.jsp?pContacto=' + Dato;
                }
            } else {
                Dato = document.getElementById('edValor').value;
                if (confirm('El nï¿½mero ingresado no existe.\nï¿½Desea ingresarlo como prospecto?'))
                    document.location.href = '../gestiones/inicioProspecto.jsp?pContacto=' + Dato;
                //Mensaje('no es valido, mando a prospectos');
            }
        }
    } else {
        Mensaje('Por favor, intente nuevamente');
    }
}


function Mensaje(msj) {
    document.getElementById('clMensajes').className = 'message';
    document.getElementById('clMensajes').innerHTML = msj;
}


function getDatosTerminal() {

    if (limpiarTerminal == 1) {
        if (busquedaCompleta == 1) {
            var dt = document.getElementById('edValor').value;
            var tdt = document.getElementById('cbCriterio').value;
            if (tdt != 'CL') {
                if ((dt != null) && (dt != '')) {
                    operacion = 'Datos Terminal';
                    sendRequest('../ObtenerDatos', 'op=term&td=' + tdt + '&d=' + dt, 'POST');
                }
            } else {
                Mensaje('No disponible en bï¿½squeda por cliente.');
            }
        } else {
            Mensaje('Debe realizar una busqueda');
        }
    } else {
        CerrarTerminal();
    }

}

//DATOS TERMINAL EN TRAFICO JCASTELLANOS DIC/2015

function getDatosTerminalTrafico() {

    if (limpiarTerminalTrafico == 1) {
        if (busquedaCompleta == 1) {
            var dt = document.getElementById('edValor').value;
            var tdt = document.getElementById('cbCriterio').value;
            if (tdt != 'CL') {
                if ((dt != null) && (dt != '')) {
                    operacion = 'Datos Terminal Trafico';
                    sendRequest('../ObtenerDatos', 'op=termtraf&td=' + tdt + '&d=' + dt, 'POST');
                }
            } else {
                Mensaje('No disponible en bï¿½squeda por cliente.');
            }
        } else {
            Mensaje('Debe realizar una busqueda');
        }
    } else {
        CerrarTerminalTrafico();
    }

}
/**************************************************/


function ObtenerDatosTerminal(xml) {
    if (ObtenerValor(xml[0].getElementsByTagName('codigo-error')) == '0') {
        var doc = xml[0].getElementsByTagName('equipo');
        DatosTerminal(doc);
    } else {
        Mensaje('No se pudo obtener datos de la terminal del telefono ingresado');
    }
}

//DATOS TERMINAL TRAFICO JCASTELLANOS DIC/2015
function ObtenerDatosTerminalTrafico(xml) {
    if (ObtenerValor(xml[0].getElementsByTagName('codigo-error')) == '0') {
        var doc = xml[0].getElementsByTagName('TerminalTrafico');
        DatosTerminalTrafico(doc);
    } else {
        Mensaje('No se pudo obtener datos de la terminal del telefono ingresado');
    }
}

function getDatosFacturacion() {
    if (limpiarFacturacion == 1) {
        if (busquedaCompleta == 1) {
            var dt = document.getElementById('edValor').value;
            var tdt = document.getElementById('cbCriterio').value;

            if ((dt != null) && (dt != '')) {
                operacion = 'Datos Facturacion';
                sendRequest('../ObtenerDatos', 'op=fac&td=' + tdt + '&d=' + dt, 'POST');
            }
        } else {
            Mensaje('Debe realizar una busqueda');
        }
    } else {
        CerrarFacturacion();
    }
}

function ObtenerDatosFacturacion(xml) {
    if (ObtenerValor(xml[0].getElementsByTagName('codigo-error')) == '0') {
        var doc = xml[0].getElementsByTagName('dir_fact');
        DireccionFacturacion(doc);
    } else {
        Mensaje('No se pudo obtener datos de la direccion de facturacion del telefono ingresado');
    }
}

function getDatosRecaudacion() {

    if (limpiarRecaudacion == 1) {
        if (busquedaCompleta == 1) {
            var dt = document.getElementById('edValor').value;
            var tdt = document.getElementById('cbCriterio').value;

            if ((dt != null) && (dt != '')) {
                operacion = 'Datos Recaudacion';
                sendRequest('../ObtenerDatos', 'op=rec&td=' + tdt + '&d=' + dt, 'POST');
            }
        } else {
            Mensaje('Debe realizar una busqueda');
        }
    } else {
        CerrarRecaudacion();
    }
}

function ObtenerInformacionRecaudacion(xml) {
    if (ObtenerValor(xml[0].getElementsByTagName('codigo-error')) == '0') {
        var doc = xml[0].getElementsByTagName('inf_rec');
        InformacionRecaudacion(doc);
    } else {
        Mensaje('No se pudo obtener informacion de recaudacion');
    }
}



function getDatosUltimasInt() {
    if (limpiarUltimasInt == 1) {
        if (busquedaCompleta == 1) {
            var fac = document.getElementById('dvCasos');
            fac.innerHTML = htmlUltimasInt;
            fac.className = claseUltimasInt;
            limpiarUltimasInt = 0;
            document.getElementById('ifrCasos').src = srcUltimasInt;
        } else {
            Mensaje('Debe realizar una busqueda');
        }
    } else {
        CerrarUltimasInt();
    }
}

function getComentario(cl) {
    operacion = 'Ultimo Comentario';
    sendRequest('../ObtenerDatos', 'op=ulcm&cl=' + cl, 'POST');
}

function ObtenerUltimoComentario(xml) {
    if (ObtenerValor(xml[0].getElementsByTagName('codigo-error')) == '0') {
        document.getElementById('edComentario').value = ObtenerValor(xml[0].getElementsByTagName('comentario'));
    } else {
        Mensaje('No se pudo obtener el ultimo comentario del cliente');
    }
}

function BuscarVisita() {
    operacion = 'Buscar Visita';
    sendRequest('../ObtenerDatos', 'op=bv', 'POST');
}

function operResVisita(xml) {
    if (ObtenerValor(xml[0].getElementsByTagName('codigo-error')) == '0') {
        var id_visita = ObtenerValor(xml[0].getElementsByTagName('id_visita'));
        var no_visita = ObtenerValor(xml[0].getElementsByTagName('no_visita'));
        document.getElementById('form1:txtNoVisita').value = no_visita;
    } else {
        document.getElementById('form1:txtNoVisita').value = '---';
        Mensaje('No tiene visita asignada.');
    }
}

function CrearVisita() {
    operacion = 'Crear Visita';
    sendRequest('../ObtenerDatos', 'op=cv', 'POST');
}

function operCrearVisita(xml) {

    if (ObtenerValor(xml[0].getElementsByTagName('codigo-error')) == '0') {
        var id_visita = ObtenerValor(xml[0].getElementsByTagName('id_visita'));
        var no_visita = ObtenerValor(xml[0].getElementsByTagName('no_visita'));
        document.getElementById('form1:txtNoVisita').value = no_visita;
    } else if (ObtenerValor(xml[0].getElementsByTagName('codigo-error')) == '2') {
        Mensaje('Ya tiene una visita asignada.');
    } else {
        document.getElementById('form1:txtNoVisita').value = '---';
        Mensaje('Ocurriï¿½ un error al crear la visita.');
    }
}

function CerrarVisita() {
    operacion = 'Cerrar Visita';
    sendRequest('../ObtenerDatos', 'op=cerrarv', 'POST');
}

function operCerrarVisita(xml) {

    if (ObtenerValor(xml[0].getElementsByTagName('codigo-error')) == '0') {
        document.location = '../gestiones/asignaVisita.jsp';
    } else {
        Mensaje('Ocurriï¿½ un error al cerrar la visita.');
    }

}