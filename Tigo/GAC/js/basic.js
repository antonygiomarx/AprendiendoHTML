jQuery(function ($) {
    // Load dialog on page load
    //$('#basic-modal-content').modal();

    // Load dialog on click
    $('#basic-modal .basic').click(function (e) {
        $('#basic-modal-content').modal();

        return false;
    });
});

var promoGestionid = null;
var numGestionid = null;
var tipologiaid = null;

function promosActivas() {
    //jQuery('#modalPromo').modal();
    jQuery('#basic-modal-content-promo').modal();
    if (promosHTML && promosHTML != null)
        document.getElementById('basic-modal-content-promo').innerHTML = promosHTML;
}

function aceptarPromo(idpromo, tipologia, numero) {
    promoGestionid = idpromo;
    numGestionid = numero;
    tipologiaid = tipologia;
    jQuery.modal.close();
    jQuery('#basic-modal-content-promo-acept').modal();

}

function rechazarPromo(idpromo, tipologia, numero) {
    promoGestionid = idpromo;
    numGestionid = numero;
    tipologiaid = tipologia;
    jQuery.modal.close();
    jQuery('#basic-modal-content-promo-rech').modal();
}

function sendPromo(comentario, motivo, tipoGestion, codCliente) {
    if (comentario == null || comentario == '') {
        alert('Debes ingresar un comentario');
        return;
    }
    sendRequest('../GestionesPromos', 'numGestion=' + numGestionid +
        '&promoGestion=' + promoGestionid +
        '&tipoGestion=' + tipoGestion +
        '&idMotivo=' + motivo +
        '&comentario=' + comentario +
        '&tipologiaid=' + tipologiaid +
        '&codCliente=' + codCliente, 'POST');

    jQuery.modal.close();
    Buscar();

}

function blink(time, interval) {
    var timer = window.setInterval(function () {
        jQuery("#imgPromoBtn").fadeOut(1500);
        window.setTimeout(function () {
            jQuery("#imgPromoBtn").fadeIn(500);
        }, 100);
    }, interval);
    window.setTimeout(function () {
        clearInterval(timer);
    }, time);
}