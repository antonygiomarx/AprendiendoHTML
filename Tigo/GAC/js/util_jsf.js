var gstrOpcionesPopUp = "dependent=yes, resizable=no, toolbar=no, status=no, directories=no, menubar=no, scrollbars=yes";
var gstrOpcionesPopUpNC = "dependent=yes, resizable=no, toolbar=no, status=no, directories=no, menubar=no, scrollbars=yes";
var gbolAbriendoPopUp = false;
var gVentPopUp = null;
var gintRatonX = 0;
var gintRatonY = 0;


// Guarda la posicion del rat?n originada por un evento
function GuardarPosRaton(pobjEvento) {
	if (navigator.appName.indexOf("Microsoft") != -1)
		pobjEvento = window.event;
	gintRatonX = pobjEvento.screenX;
	gintRatonY = pobjEvento.screenY;
}

// Abre ventana "POP UP"
function AbrirPopUp(pstrUrl, pstrNombre, pintAncho, pintAlto, pstrOpcs, pbolRelativo) {
	CerrarPopUp();
	gbolAbriendoPopUp = true;
	if (pbolRelativo) {
		var intRatonX = gintRatonX;
		var intRatonY = gintRatonY;

		if (intRatonX - pintAncho < 0)
			intRatonX = parseInt(pintAncho);

		if (parseInt(intRatonY) + parseInt(pintAlto) > screen.height) {
			intRatonY -= (parseInt(intRatonY) + parseInt(pintAlto) + 50) - parseInt(screen.height);
		}
		intRatonX -= pintAncho;
		intRatonY += 10;

		if (pstrOpcs != "")
			pstrOpcs += ",";

		pstrOpcs += " width=" + pintAncho + ", height=" + pintAlto + ", screenX=" + intRatonX + ", left=" + intRatonY + ", top=" + intRatonY + ", alwaysRaised";
	}

	gVentPopUp = window.open(pstrUrl, pstrNombre, pstrOpcs, true);
	gVentPopUp.focus();

	gbolAbriendoPopUp = false;
}

// Cierra ventana "POP UP"
function CerrarPopUp() {
	if (gVentPopUp != null)
		if (!gVentPopUp.closed)
			gVentPopUp.close();
	gVentPopUp = null;

}

/*
      Esta funcion devuelve un mensaje de confirmación al usuario, solo si se encuentra 
      seleccionado al menos un rowSelect de la tabla sobre la cual se desea borrar algun elemento
            recibe  nombre: nombre del rowSelect de la tabla
                        form: id formulario
                        msg:    mensaje de confirmación al usuario
*/

function msgBorrarRowSelect(nombre, form, msg) {
	var elementos;
	var i;
	elementos = document.forms[form].elements;
	for (i = 0; i < elementos.length; i++)
		if (elementos[i].name.indexOf(nombre + "__hidden_sel") != -1)
			if (elementos[i].value == "true")
				return confirm("¿Está seguro de borrar los registros seleccionados?");
	alert("No existen elementos seleccionados para borrar");
	return false;
}

function msgAutorizarRowSelect(nombre, form, msg) {
	var elementos;
	var i;
	elementos = document.forms[form].elements;
	for (i = 0; i < elementos.length; i++)
		if (elementos[i].name.indexOf(nombre + "__hidden_sel") != -1)
			if (elementos[i].value == "true")
				return confirm("¿Está seguro de autorizar los registros seleccionados?");
	alert("No existen elementos seleccionados");
	return false;
}

function msgNoAutorizarRowSelect(nombre, form, msg) {
	var elementos;
	var i;
	elementos = document.forms[form].elements;
	for (i = 0; i < elementos.length; i++)
		if (elementos[i].name.indexOf(nombre + "__hidden_sel") != -1)
			if (elementos[i].value == "true")
				return confirm("¿Está seguro de no autorizar los registros seleccionados?");
	alert("No existen elementos seleccionados");
	return false;
}

function msgFinalizarRowSelect(nombre, form, msg) {
	var elementos;
	var i;
	elementos = document.forms[form].elements;
	for (i = 0; i < elementos.length; i++)
		if (elementos[i].name.indexOf(nombre + "__hidden_sel") != -1)
			if (elementos[i].value == "true")
				return "";
	alert("No hay elementos seleccionados para finalizar");
	return false;
}

function msgRowSelect(nombre, form, pregunta, mensaje) {
	var elementos;
	var i;
	elementos = document.forms[form].elements;
	for (i = 0; i < elementos.length; i++)
		if (elementos[i].name.indexOf(nombre + "__hidden_sel") != -1)
			if (elementos[i].value == "true")
				return confirm(pregunta);
	alert(mensaje);
	return false;
}


function deshabilitaAyudaCalendario(form, calendario) {
	var elementos;
	elementos = document.forms[form].elements;
	for (var i = 0; i < elementos.length; i++) {
		if (elementos[i].name.indexOf(calendario) != -1)
			elementos[i + 1].disabled = true;
	}
	return false;
}

function endsWith(testString, endingString) {
	if (endingString.length > testString.length) return false;
	return testString.indexOf(endingString) == (testString.length - endingString.length);
}

function IniciarOverlay() {
	jQuery(document).ready(function () {
		jQuery("#facebox").overlay({
			// custom top position
			top: "center",
			//custom efect
			effect: "apple",
			fixed: false,
			speed: "fast",
			// some mask tweaks suitable for facebox-looking dialogs
			mask: {
				// you might also consider a "transparent" color for the mask
				color: '#595959',
				// load mask a little faster
				loadSpeed: 200,
				// very transparent
				opacity: 0.5
			},
			// disable this for modal dialog-type of overlays
			closeOnClick: false,
			closeOnEsc: false,
			onBeforeLoad: function () {
				jQuery('body').css("overflow", "hidden");
			},
			onClose: function () {
				jQuery('body').css("overflow", "auto");
			},
			// load it immediately after the construction
			load: false,
			api: true
		});
	});
}

function func_overlay(thisObj, thisEvent) {
	jQuery("#facebox").overlay().load();
}

function validaBusquedaNumero() {

	jQuery('#errors').html('');

	vNum = true;

	jQuery('.numero').each(function () {
		var numero = jQuery(this);

		if (vNum && !numero.val().toString().length == 0) {
			if (vNum && !isNumber(numero.val())) {
				message = 'El código a buscar debe ser un número entero.\n';
				vNum = false;
			}
		}
	});

	//valida busqueda número o %
	jQuery('.numeroPorcentaje').each(function () {
		var numero = jQuery(this);

		if (vNum && !numero.val().toString().length == 0) {
			if (vNum && !isNumberOrAll(numero.val())) {
				message = 'El código a buscar debe ser un número entero.\n';
				vNum = false;
			}
		}
	});

	//solo para limpiar viejos mensajes de error
	if (jQuery('.messages').length > 0) {
		jQuery('.messages').each(function () {
			var $this = jQuery(this);
			$this.text("");
		});
	}

	if (!vNum) {
		jQuery('#errors').html(message);
		return false;
	} else {
		jQuery('#errors').html('');
		return true;
	}
};

function isNumber(valor) {
	var ExpReg = /^([0-9])*$/;
	if (ExpReg.test(valor)) {
		return true;
	} else {
		return false;
	}
};

function isNumberOrAll(valor) {
	var ExpReg = /^([0-9])*$|^%$/;
	if (ExpReg.test(valor)) {
		return true;
	} else {
		return false;
	}
};