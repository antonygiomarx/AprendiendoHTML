// request.js



var READY_STATE_UNINITIALIZED = 0;
var READY_STATE_LOADING = 1;
var READY_STATE_LOADED = 2;
var READY_STATE_INTERACTIVE = 3;
var READY_STATE_COMPLETE = 4;
var req;
var xmlRespuesta;
var operacion = '';


// getRequest
function getRequest() {
    var xRequest = null;
    if (window.XMLHttpRequest) {
        xRequest = new XMLHttpRequest();
    } else if (typeof ActiveXObject != "undefined") {
        xRequest = new ActiveXObject("Microsoft.XMLHTTP");
    }
    return xRequest;
}

// sendRequest
function sendRequest(url, params, HttpMethod) {
    if (!HttpMethod) {
        HttpMethod = "POST";
    }
    req = getRequest();
    if (req) {
        req.onreadystatechange = onReadyState;
        req.open(HttpMethod, url, true);
        req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        req.send(params);
    }
}


function onReadyState() {
    var ready = req.readyState;
    var res = null;
    var data = null;
    if (ready == READY_STATE_COMPLETE) {
        res = req.responseText;
        //req.responseText = req.responseText.replace(res.substring(res.indexOf('<promos>'),res.indexOf('</promos>')),''); 
        xmlRespuesta = getXMLDocument(req.responseText);
        Mensaje('');
        if (operacion == 'Datos Generales') {
            if (document.getElementById('cbCriterio').value == 'CL') {
                if ((ObtenerValor(xmlRespuesta.getElementsByTagName('cuenta')[0].getElementsByTagName('nombre')) != null) &&
                    (ObtenerValor(xmlRespuesta.getElementsByTagName('cuenta')[0].getElementsByTagName('nombre')) != 'null')) {
                    DatosCuenta(xmlRespuesta.getElementsByTagName('cuenta'));
                    DatosCliente(xmlRespuesta.getElementsByTagName('cliente'));
                } else {
                    Mensaje('Por favor, intente nuevamente');
                }
            } else {
                DatosCuenta(xmlRespuesta.getElementsByTagName('cuenta'));
                DatosCliente(xmlRespuesta.getElementsByTagName('cliente'));
                res = req.responseText;

                promosHTML = res.substring(res.indexOf('<promos>') + 8, res.indexOf('</promos>'));
                DatosAbonado(xmlRespuesta.getElementsByTagName('abonado'));
            }

            operacion = '';
        }
        if (operacion == 'Telefono Existe') {
            ProcesaTelefono(xmlRespuesta.getElementsByTagName('respuesta'));
        }

        if (operacion == 'Validar Telefono') {
            TelefonoValido(xmlRespuesta.getElementsByTagName('respuesta'));
        }
        if (operacion == 'Datos Terminal') {
            ObtenerDatosTerminal(xmlRespuesta.getElementsByTagName('respuesta'));
            operacion = '';
        }
        //DATOS TERMINAL EN TRAFICO 
        if (operacion == 'Datos Terminal Trafico') {
            ObtenerDatosTerminalTrafico(xmlRespuesta.getElementsByTagName('respuesta'));
            operacion = '';
        }

        if (operacion == 'Datos Facturacion') {
            ObtenerDatosFacturacion(xmlRespuesta.getElementsByTagName('respuesta'));
            operacion = '';
        }
        if (operacion == 'Ultimo Comentario') {
            ObtenerUltimoComentario(xmlRespuesta.getElementsByTagName('respuesta'));
            operacion = '';
        }
        if (operacion == 'Buscar Visita') {
            operResVisita(xmlRespuesta.getElementsByTagName('respuesta'));
            operacion = '';
        }
        if (operacion == 'Crear Visita') {
            operCrearVisita(xmlRespuesta.getElementsByTagName('respuesta'));
            operacion = '';
        }
        if (operacion == 'Cerrar Visita') {
            operCerrarVisita(xmlRespuesta.getElementsByTagName('respuesta'));
            operacion = '';
        }
        if (operacion == 'Datos Recaudacion') {
            ObtenerInformacionRecaudacion(xmlRespuesta.getElementsByTagName('respuesta'));
            operacion = '';
        }

    } else {
        Mensaje('Realizando consulta...');
    }

}

/*function getXMLDocument()
{
	var xDoc=null;
	if (document.implementation && document.implementation.createDocument)
	{
		xDoc=document.implementation.createDocument("","",null);
	}else if (typeof ActiveXObject != "undefined")
	{
		var msXmlAx==null;
		try{
			msXmlAx=new ActiveXObject("Msxml2.DOMDocument");
		}catch (e)
		{
			msXmlAx=new ActiveXObject("Msxml.DOMDocument");
		}
		xDoc=msXmlAx;
	}
	if (xDoc==null || typeof xDoc.load=="undefined")
	{	xDoc=null; }
	return xDoc;
}
*/

function getXMLDocument(text) {
    var xmlDoc = null;

    try //Internet Explorer
    {
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.async = "false";
        xmlDoc.loadXML(text);

    } catch (e) {
        try //Firefox, Mozilla, Opera, etc.
        {
            parser = new DOMParser();
            xmlDoc = parser.parseFromString(text, "text/xml");
        } catch (e) {
            alert(e);
        }
    }

    return xmlDoc;
}